package main

import (
	"os"
)

func numberToWord(number uint) string {
	units := []string{"one", "ten", "hundred", "thousand", "million"}
	numbers := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	teens := []string{"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"}
	tens := []string{"ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"}
	result := ""

	if number == 0 {
		return "zero"
	}
	if number >= 1000000 {
		result += numberToWord(number / 1000000)
		result += units[4] + " "
		number = number - ((number / 1000000) * 1000000)
	}
	if number >= 1000 {
		result += numberToWord(number / 1000)
		result += units[3] + " "
		number = number - ((number / 1000) * 1000)
	}
	if number >= 100 {
		result += numbers[number/100-1] + " " + units[2] + " "
		number = number - ((number / 100) * 100)
	}
	if number >= 10 && number < 20 {
		number = number - 10
		result += teens[number] + " "
	} else {
		if number >= 20 {
			result += tens[number/10-1] + " "
			number -= (number / 10 * 10)
		}
		if number >= 1 {
			result += numbers[number-1] + " "
		}
	}

	return result
}

func main() {
	out, _ := os.Create("out.txt")
	for i := uint(0); i <= 1000000; i++ {
		out.WriteString(numberToWord(i) + "\n")
	}
	out.Close()
}
